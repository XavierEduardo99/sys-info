# ![Main icon](./Images/Icon64.png) System information



[MASTER-Branch](https://gitlab.com/XavierEduardo99/sys-info/-/tree/master)

This bash script will help the user monitor the hardware and sofware of their GNU/Linux system by providing the following information:
  + Currently logged user.
  + Uptime of the system.
  + Processes active.
  + Memory used and free.
  + Percentage of CPU idling.
  + State of storage.
  + Public IP address.
  
![Screenshot](./Images/Screenshot.png)

## Dependencies:
In order to run this project you will need the following packages:
  + GNU Coreutils
  + cURL

## Usage guide: 

+ To get a summary of your system, run:
```sh
$ chmod +x sys-info.sh && ./sys-info.sh
```

## Install guide: 

+ To install this software in your computer, run:
```sh
$ sudo cp -v sys-info.sh /usr/local/bin/sys-info
$ sudo chmod +x /usr/local/bin/sys-info
$ gzip -9n sys-info.1 
$ sudo mv sys-info.1.gz /usr/share/man/man1/
```

### It is suggested to add this software to autorun everytime you log in.

## Contact with the developer
| Contact | Link |
| ------ | ------ |
| My Mail | xaviervintimilla@yahoo.es |

