#!/bin/bash
#
#
#   Copyright (c) 2023, Xavier Vintimilla
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright notice, this
#      list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#
#   3. Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Define colors
GREEN='\033[0;32m'
RESET='\033[0m'
BOLD='\033[1m'

# Get the system information
CURRENT_DATE="$(date +'%a %d/%m/%Y | %H:%M:%S')"									# Format date and time
CURRENT_KERNEL="$(uname -mrs)"														# Get the current version of the kernel
USER="$(whoami)"																	# Current user
UPTIME="$(uptime -p)"																# System uptime
TOTAL_MEMORY="$(free -h | sed -n 2p | awk '{print $2}')"							# Get the total memory in GiB
USED_MEMORY="$(free -h | sed -n 2p | awk '{print $3}')"								# Get the used memory in GiB
CPU="$(vmstat 1 2 | sed -n '/[0-9]/p' | sed -n '2p' | awk '{print $15}')"			# Get the percentage of CPU that is idling
SPACE_LEFT="$(df -h / | awk '{ a = $4 } END { print a }')"							# Get the space left in the root partition
SPACE_USED="$(df -h / | awk '{ a = $3 } END { print a }')"							# Get the space used of the root partition
PERCENTAGE_USED="$(df -h / | awk '{ a = $5 } END { print a }')"						# Get the percentage used of the root partition
RUNNING_PROCESSES="$(ps -e | wc -l)"												# Get the number of processes running in the system
PUBLIC_IP="$(curl ifconfig.me 2> /dev/null)"										# Get the current public IP address

function showSystemInformation(){
	echo
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} System information at:${RESET}      ${CURRENT_DATE}"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Currently used kernel:${RESET}      ${CURRENT_KERNEL}"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Currently logged user:${RESET}      ${USER}"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Uptime of the system:${RESET}       ${UPTIME}"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Processes active:${RESET}           ${RUNNING_PROCESSES}"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Memory used and free:${RESET}       ${USED_MEMORY}B of ${TOTAL_MEMORY}B"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Percentage of CPU idling:${RESET}   ${CPU}%"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} State of storage:${RESET}           ${SPACE_USED}B used of ${SPACE_LEFT}B (${PERCENTAGE_USED})"
	echo -e " ${BOLD}${GREEN}*${RESET}${BOLD} Public IP address:${RESET}          ${PUBLIC_IP}"
	echo
}

showSystemInformation
exit 0
